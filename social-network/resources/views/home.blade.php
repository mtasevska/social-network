@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row d-flex justify-content-around">
            <div class="col-md-9">
                <img src="{{ auth()->user()->url }}" style="width: 150px" class="d-inline profile-picture">
                <p class="d-inline profile-name font-weight-bold ml-3">{{ auth()->user()->name }}</p>
            </div>

            <div class="col-md-3 mt-5">
                <button type="button" data-toggle="modal" data-target="#postModal" class="btn btn-primary post-button">Add a
                    new Post</button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 offset-2">
                @foreach ($posts as $post)
                    @if (auth()->user()->id === $post->user_id)

                        <div class="card mt-5 mb-5">
                            <div class="card-body">
                                <h5 class="card-title ml-5 font-weight-bold">{{ $post->user->name }}</h5>
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="{{ $post->user->url }}" style="width: 50px">
                                    </div>
                                    <div class="col-md-9">
                                        {{ $post->content }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-warning mr-3" data-toggle="modal"
                                        data-target="#editModal">Edit</button>
                                    <button type="submit" class="btn btn-danger mr-3" data-toggle="modal"
                                        data-target="#deleteModal">Delete</button>
                                </div>

                                <!-- Edit Modal -->
                                <div class="modal fade" id="editModal" tabindex="-1" role="dialog"
                                    aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Post</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('update', $post->id) }}" method="post">
                                                @csrf
                                                <div class="modal-body">
                                                    <textarea name="updateModal" id="updateModal" cols="55"
                                                        rows="3">{{ $post->content }}</textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger">Update</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Delete Modal -->
                            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                                aria-labelledby="deleteModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Delete Post</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to delete this post?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <form action="{{ route('delete', $post->id) }}" method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 font-italic">
                                <p class="ml-5 mt-1">{{ $post->created_at }}</p>
                            </div>
                        </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>

    </div>

    {{-- MODAL --}}

    <div class="modal fade" id="postModal" tabindex="-1" aria-labelledby="postModal" aria-hidden="true">
        <form action="{{ route('store', $post->id) }}" method="POST">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add a new post</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <textarea class="form-control" name="content" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Post</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
