<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::all();

        return view('home', compact('posts'));
    }

    public function store(Request $request, $id)
    {
        $content = $request->get('content');
        Post::create([
            'content' => $content,
            'user_id' => Auth::user()->id
        ]);

        return redirect()->back();
    }

    public function delete($id) {
        $deletedRows = Post::where('id', $id)->delete();
        return redirect()->back()->with('success', 'Post Deleted.');

    }

    public function update(Request $request, $id)
    {
        Post::where('id', $id)
        ->update(['content' => $request->input('updateModal')]);
        return redirect()->back();
    }


}
